package com.db.parcial1v2.Model;

import android.widget.Spinner;

public class Empleado {
    private String name;
    private Double tiempoLaborado;
    private Double salario;
    private String mes;

    public Empleado() {
        this.name = name;
        this.tiempoLaborado = tiempoLaborado;
        this.salario = salario;
        this.mes = mes;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getTiempoLaborado() {
        return tiempoLaborado;
    }

    public void setTiempoLaborado(Double tiempoLaborado) {
        this.tiempoLaborado = tiempoLaborado;
    }

    public Double getSalario() {
        return salario;
    }

    public void setSalario(Double salario) {
        this.salario = salario;
    }

    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }
}
