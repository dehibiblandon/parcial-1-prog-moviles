package com.db.parcial1v2;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.db.parcial1v2.Model.Empleado;

import java.util.ArrayList;
import java.util.LinkedList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    private Button calcularSalario;
    private Button verSal;
    private EditText nombreEmpleado;
    private EditText salarioEmpleado;
    private Spinner mesSpinner;
    private EditText tiempoLaborado;
    private ArrayList<String> meses = new ArrayList<>();
    private ArrayAdapter adapter;
    private ArrayList<Empleado> empleados;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        nombreEmpleado = findViewById(R.id.nameEmpleado);
        salarioEmpleado = findViewById(R.id.salarioEmpleado);
        tiempoLaborado = findViewById(R.id.tiempoLaborado);
        mesSpinner = (Spinner) findViewById(R.id.listadoMes);
        empleados = new ArrayList<>();
        calcularSalario = findViewById(R.id.btnCalcular);
        calcularSalario.setOnClickListener(this);

        verSal = findViewById(R.id.verSalarios);
        verSal.setOnClickListener(this);

        meses.add("Enero");
        meses.add("Febrero");
        meses.add("Marzo");
        meses.add("Abril");
        meses.add("Mayo");
        meses.add("Junio");
        meses.add("Julio");
        meses.add("Agosto");
        meses.add("Septiembre");
        meses.add("Octubre");
        meses.add("Noviembre");
        meses.add("Diciembre");

        adapter = new ArrayAdapter(MainActivity.this, android.R.layout.simple_spinner_dropdown_item, meses);
        mesSpinner.setAdapter(adapter);
        mesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String mesSeleccionado = (String) mesSpinner.getAdapter().getItem(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }//end on create

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnCalcular:
                Empleado empleado = new Empleado();
               /* empleado.setName(nombreEmpleado.getText().toString());
                empleado.setSalario(Double.valueOf(salarioEmpleado.getText().toString()));
                empleado.setTiempoLaborado(Double.valueOf(tiempoLaborado.getText().toString()));
                empleado.setMes((String) mesSpinner.getSelectedItem());*/
                empleado.setName("Juan Perez");
                empleados.add(empleado);

                if(empleados.add(empleado)){
                    /*Toast.makeText(this, "el Nombre del Trabajador es" + nombreEmpleado.getText().toString() + salarioEmpleado.getText().toString()
                            + tiempoLaborado.getText().toString() + mesSpinner.getSelectedItem(), Toast.LENGTH_LONG).show();*/
                }else{
                    Toast.makeText(this, "NO se registraron datos", Toast.LENGTH_LONG).show();
                }
                this.clearForm();

                break;
            case R.id.verSalarios:
                Bundle extra = new Bundle();
                extra.putSerializable("lista", empleados);
                Intent intent  = new Intent(MainActivity.this, CalcularSalario.class);
                intent.putExtra("extra", extra);
                startActivity(intent);
                break;
        }
    }//END onClick



    public void clearForm(){
        nombreEmpleado.getText().clear();
        salarioEmpleado.getText().clear();
        tiempoLaborado.getText().clear();
    }
}