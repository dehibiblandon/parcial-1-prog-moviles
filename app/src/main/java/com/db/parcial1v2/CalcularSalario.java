package com.db.parcial1v2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.db.parcial1v2.Model.Empleado;

import java.util.ArrayList;

public class CalcularSalario extends AppCompatActivity implements View.OnClickListener {
    private Button volverInicio;
    private TextView NombreEmpleado;
    private TextView SalarioEmpleado;
    private TextView Prima;
    private TextView Vacaciones;
    private TextView Pension;
    private TextView Cesantias;
    private TextView Salud;
    private TextView AntiguedadEmpleado;
    private TextView Mes;
    private ArrayList<Empleado> listEmpleado = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calcular_salario);
        NombreEmpleado = findViewById(R.id.textNombreEmpleado);
        SalarioEmpleado = findViewById(R.id.textSalarioEmpleado);
        Prima = findViewById(R.id.textPrima);
        Vacaciones = findViewById(R.id.textVacaciones);
        Pension = findViewById(R.id.textPension);
        Cesantias = findViewById(R.id.texCesantias);
        Salud = findViewById(R.id.textSalud);
        AntiguedadEmpleado = findViewById(R.id.textAntiguedadEmpleado);
        Mes =  findViewById(R.id.valorMes);

        Bundle intent = getIntent().getBundleExtra("extra");
        listEmpleado= (ArrayList<Empleado>) (intent.getSerializable("lista"));
       /* NombreEmpleado.setText(listEmpleado.get(0).getName().toString());

        Double valorPrima = calcularPrima (listEmpleado.get(0).getSalario(), listEmpleado.get(0).getTiempoLaborado());
        Prima.setText(valorPrima.toString());*/
        volverInicio = findViewById(R.id.volverHome);

        volverInicio.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.volverHome:
                Intent intent  = new Intent(CalcularSalario.this, MainActivity.class);
                startActivity(intent);
                break;
        }
    }
    public Double calcularPrima(double salario, double tiempo){
        double  valorPrima = 10;
        return valorPrima;
    }
}